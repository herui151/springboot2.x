《SpringBoot之MybatisPlus实战源码集》是以`合集`的形式系统讲述`MybatisPlus`在实战中的使用，相较于零散式的视频分享，它的系统性更强，学习价值更大。

本合集既可以作为日常学习使用，也可以作为日常开发过程中参考源代码。

由于视频制作较慢，这里提前将合集内容分享出来。正如已经存在视频的演示那样，源代码中包含：源代码、数据库表结构、演示demo数据、配套说明文档

涉及到有赛泰先生开源发布的Maven包，由于版本持续在迭代，兼容可能会存在一些影响，但并不影响源代码的学习与使用。

源代码均能够启动运行，将示例代码移植到其它项目中需要做些兼容性调试，它不是演示源代码的问题。


---

| 模块名   | 实战内容   | 视频讲解 |
| --- | --- | --- |
| 04001-springboot-mybatis-plus-intro |快速入门 | [传送门]() |
| 04002-springboot-mybatis-plus-auto-fill-metainfo |字段属性全局自动填充 |[传送门](https://www.bilibili.com/video/BV17X4y1S7a5)|
| 04003-springboot-mybatis-plus-enum |字段枚举 |[传送门](https://www.bilibili.com/video/BV1Dk4y1h7TR)|
| 04004-springboot-mybatis-plus-typehandle |类型处理器 处理复杂类型 |[传送门]()|
| 04005-springboot-mybatis-plus-custom-method |自定义Mapper方法 |[传送门]()|
| 04006-springboot-mybatis-plus-tenant | 多租户|[传送门]()|
| 04007-springboot-mybatis-plus-active-record |AR |[传送门]()|
| 04008-springboot-mybatis-plus-id-generator |自定义ID生成器 |[传送门]()|
| 04009-springboot-mybatis-plus-id-string | 自定义ID生成器（字符串）|[传送门]()|
| 04010-springboot-mybatis-plus-resultmap |处理复杂返回值 |[传送门]()|
| 04011-springboot-mybatis-plus-pagehelper |整合pagehelper分页 |[传送门]()|
| 04012-springboot-mybatis-plus-pagination |分页 |[传送门]()|
| 04013-springboot-mybatis-plus-logic-delete |逻辑删除 |[传送门]()|
| 04014-springboot-mybatis-plus-optimistic-locker |乐观锁 |[传送门]()|
| 04015-springboot-mybatis-plus-wrapper | 查询条件构造器|[传送门]()|
| 04016-springboot-mybatis-plus-execution-protect |执行保护 |[传送门]()|
| 04017-springboot-mybatis-plus-sequence |oracle等数据库保持顺序 |[传送门]()|
| 04020-springboot-mybatis-plus-dao-cache |DAO层添加缓存 |[传送门](https://www.bilibili.com/video/BV1dM411m7RG)|
| 04021-springboot-mybatis-plus-dao-cache-lock |DAO通过加锁添加缓存 |[传送门]()|
| 04022-springboot-mybatis-plus-business-report |复杂业务报表开发 |[传送门]()|
| 04023-springboot-mybatis-plus-lambda-groupby |聚合函数lambda版实战 |[传送门](https://www.bilibili.com/video/BV1Gt4y1K7x1)|
| 04024-springboot-mybatis-plus-lambda-count-sum-avg-min-max |多种类型聚合函数lambda版实战 |[传送门](https://www.bilibili.com/video/BV1324y1f726)|
| 04025-springboot-mybatis-plus-id-to-name |lambda版将ID字段替换成name字段 |[传送门](https://www.bilibili.com/video/BV1cg411B7Z8)|
| 04026-springboot-mybatis-plus-pk-backfill | 主键回填|[传送门](https://www.bilibili.com/video/BV1SG4y157NQ)|
| 04027-springboot-mybatis-plus-lambda-increase-decrease |lambda版自增-自减实现 |[传送门](https://www.bilibili.com/video/BV1SP411K7LZ)|
| 04028-springboot-mybatis-plus-lambda-increase-decrease2 |lambda版自增-自减实现（自定义框架实现） |[传送门]()|
| 04029-springboot-mybatis-plus-redundance-field | lambda版冗余字段实现|[传送门]()|
| 04030-springboot-mybatis-plus-redundance-field2 | lambda版冗余字段实现（自定义框架实现）|[传送门](https://www.bilibili.com/video/BV1rs4y1H7Hx)|
| 04031-springboot-mybatis-plus-redis-bitmap | MybatisPlus自定义框架封装Redis BitMap（自定义框架实现）|[传送门](https://www.bilibili.com/video/BV1Ms4y1D7ik)|