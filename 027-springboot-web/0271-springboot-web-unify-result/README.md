### Web控制器统一返回值形式

在`前后端分离`项目中，统一`JSON`数据返回值很有必要，方便`前端开发`人员与`后端开发`人员`系统对接`。

##### 1、直接返回对象实例

```java
public Object test1() {
    return user;
}
```

结果展示

```json
{
  "userId": "100",
  "userName": "Java"
}
```

直接返回对象实例常见于刚入门的新手，尽管能够返回JSON数据，然而由于其不能够控制HTTP状态码，因此几乎不使用。

##### 2、使用`ResponseEntity`包装对象实例

```java
public ResponseEntity<Object> test2() {
    return ResponseEntity.ok(user);
}
```

使用ResponseEntity包装对象，给后端开发提供管理HTTP状态码的能力，并且符合HTTP的标准，使用比较多。

常见的状态码有`2XX`、`4XX`、`5XX`

结果展示

```json
{
  "userId": "100",
  "userName": "Java"
}
```

##### 3、自定义包装对象实例

```java
public AjaxResult test3() {
    return AjaxResult.success(user);
}
```

自定义包装器默认使用`200`状态码，在其返回数据体中添加自定义状态码。

自定义包装器参考实现，引入如下依赖包，搜索`AjaxResult`类即可。

```xml
<dependency>
    <groupId>xin.altitude.cms</groupId>
    <artifactId>ucode-cms-common</artifactId>
    <version>1.6.4</version>
</dependency>
```

结果展示

```json
{
  "code": 200,
  "msg": "操作成功",
  "data": {
    "userId": "100",
    "userName": "Java"
  }
}
```

---

##### 补充

在实际开发过程中自定义包装器用的更多，原因是系统对接起来更为方便，尽管方式二更为标准。很多老开发坚持方式二的标准。


