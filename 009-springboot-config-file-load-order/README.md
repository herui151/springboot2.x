### 全局配置文件加载顺序

不同文件位置、不同文件类型全局配置文件加载顺序：先加载会被后加载覆盖!

```text
. project-sample
├── config
│   ├── application.yml （4）
│   └── src/main/resources
|   │   ├── application.yml （1）
|   │   └── config
|   |   │   ├── application.yml （2）
├── application.yml （3）
```
- `同种类型`的不同目录下配置文件的加载顺序如上所示，先加载的会被有加载的配置覆盖。

- `不同类型`的相同目录下配置文件加载顺序按照` yml  -->  yaml   -->  properties`进行，即后加载会覆盖前面加载的。


> `yml`文件中文正常读取；`properties`文件中文读取乱码 配置文件统一使用英文 养成良好的习惯

