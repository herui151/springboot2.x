## 数据库表字段加解密

对于`信息安全`有要求或者是涉及`个人敏感信息`的内容，数据库字段不得以`明文`显示，因此产生了数据库`字段加解密`的需求。

### 技术选型
SpringBoot + MyBatis + Mysql

使用MyBatis XML编写SQL脚本，调用Mysql内置的`AES函数`完成加解密操作。

具体逻辑如下：
加密：先使用`aes_encrypt`函数对明文加密，然后使用`to_base64`函数进行转码，转码后才是正常字符串
```sql
insert tb_user values(12,'test03',22,to_base64(aes_encrypt('18122223333','c86038fe04bc7097')));
```
解密：先使用`from_base64`函数进行反转码，然后使用`aes_decrypt`函数进行解密
```sql
select user_id,user_name,age,aes_decrypt(from_base64(mobile),'c86038fe04bc7097') as mobile from tb_user
```

> 密钥通过参数的形式传递

### 功能演示

数据保存前是明文状态
```json
{
  "age": 22,
  "mobile": "13012123434",
  "userId": 1,
  "userName": "admin"
}
```

数据库存储状态

![img.png](img.png)



数据查询时是明文状态，可配合数据脱敏使用，将部分字符用`*`表示。
```json
{
  "age": 22,
  "mobile": "13012123434",
  "userId": 1,
  "userName": "admin"
}
```

### 实现原理

- `保存数据`时对特定字段进行`加密`
- `查询数据`时对特定字段进行`解密`


### 关于查询
##### 精确查询
对加密字段精确查询是支持的，对应的字段属性按照加密的方式传值即可。

##### 模糊查询
对加密字段模糊查询有较大的性能损失，数据量较小是勉强能够实现，数据量较大时不推荐使用。