#
************************************************************
# Sequel Pro SQL dump
# Version 5446
#
# https://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 8.0.31)
# Database: test_075
# Generation Time: 2023-01-12 15:04:01 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
SET NAMES utf8mb4;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


#
Dump of table sys_user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sys_user`;

CREATE TABLE `sys_user`
(
    `id`          bigint NOT NULL AUTO_INCREMENT COMMENT '主键id',
    `user_name`   varchar(55) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '姓名',
    `sex`         tinyint(1) DEFAULT NULL COMMENT '性别',
    `age`         int                                                          DEFAULT NULL COMMENT '年龄',
    `address`     varchar(255)                                                 DEFAULT NULL COMMENT '地址',
    `status`      tinyint                                                      DEFAULT NULL COMMENT '状态',
    `create_time` datetime                                                     DEFAULT NULL COMMENT '创建时间',
    `update_time` datetime                                                     DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

LOCK
TABLES `sys_user` WRITE;
/*!40000 ALTER TABLE `sys_user` DISABLE KEYS */;

INSERT INTO `sys_user` (`id`, `user_name`, `sex`, `age`, `address`, `status`, `create_time`, `update_time`)
VALUES (25, '张三', 1, 12, '深圳', 1, '2023-01-12 22:31:22', NULL),
       (26, '李四', 1, 32, '广州', 1, '2023-01-12 22:31:22', NULL),
       (27, '王雪', 2, 21, '上海', 1, '2023-01-12 22:31:22', NULL),
       (28, '孙云', 2, 23, '北京', 1, '2023-01-12 22:31:22', NULL),
       (29, '赵柳', 2, 42, '成都', 1, '2023-01-12 22:31:22', NULL),
       (30, '孙雪', 2, 15, '武汉', 1, '2023-01-12 22:31:22', NULL),
       (31, '张三', 1, 12, '深圳', 1, '2023-01-12 22:32:42', NULL),
       (32, '李四', 1, 32, '广州', 1, '2023-01-12 22:32:42', NULL),
       (33, '王雪', 2, 21, '上海', 1, '2023-01-12 22:32:42', NULL),
       (34, '孙云', 2, 23, '北京', 1, '2023-01-12 22:32:42', NULL),
       (35, '赵柳', 2, 42, '成都', 1, '2023-01-12 22:32:42', NULL),
       (36, '孙雪', 2, 15, '武汉', 1, '2023-01-12 22:32:42', NULL);

/*!40000 ALTER TABLE `sys_user` ENABLE KEYS */;
UNLOCK
TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
