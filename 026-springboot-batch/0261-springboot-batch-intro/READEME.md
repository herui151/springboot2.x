## Spring Batch批处理框架

批处理广泛应用于大数据场景。

### 一、Why批处理

大数据场景，数据源的数据量一般比较大，不可能一次性将数据源数据拉到内存中。一方面硬件内存不支持，另一方面等待时间过长。

在这样的背景下，批处理被提出

### 二、批处理的特点

批处理按照一组一组的方式从数据源拉取数据到内存中，处理完成后，批量写入目标数据源。由于是一组一组的`取数据-处理数据-写数据`，即便是物理机器硬件内存不是很大，也能够处理大数据场景需求。

一般而言，大数据处理执行时间普遍比较长，中途可能会发生各种意外，那么记录运行元数据尤为必要。当发生意外时，能够从意外出继续重启执行，避免陷入从头再来的尴尬处境。

### 三、Spring Batch批处理框架

