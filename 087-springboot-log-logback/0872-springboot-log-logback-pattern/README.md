## logback日志格式

SpringBoot默认定义的日志格式非常不错，若想自定义日志格式，可在其基础上定制。

> 总体上来说，不需要修改太多的内容，尽量复用，保持风格连贯


```properties
# 设置日志运行的日期格式
logging.pattern.dateformat="yyyy-MM-dd hh:mm:ss"
```