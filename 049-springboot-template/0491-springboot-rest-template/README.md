## RestTemplate使用指南

### 一、Get请求

#### （一）返回目标对象
![img.png](img.png)

假设有如下请求，通过示例代码完成请求调用 `/test?age=12&sex=男`

##### 1、无变量参数
无变量参数或者将变量参数拼接成字符串形成无变量url请求字符串

```java
public ResponseEntity<Object> test0(UserDTO userDTO) {
    Integer age = userDTO.getAge();
    String sex = userDTO.getSex();
    String url = "http://localhost:8080/test?age="+age+"&sex="+sex;
    User user = restTemplate.getForObject(url, User.class);
    return ResponseEntity.ok(user);
}
```

此种方式通俗易懂，但需要注意拼接字符串引号

##### 2、有变量参数

通过占位符的方式注入变量

```java
public ResponseEntity<Object> test1(UserDTO userDTO) {
    String url = "http://localhost:8080/test?age={1}&sex={2}";
    User user = restTemplate.getForObject(url, User.class, userDTO.getAge(), userDTO.getSex());
    return ResponseEntity.ok(user);
}
```

占位符的编号从`1`开始，切记不是从`0`开始。

使用`占位符`的方式构造请求字符串形式比较优雅，不像`+`号拼接字符串混乱，推荐使用。

> 返回目标对象期望对方服务器总能够返回成功的结果，否则程序在运行时可能出现`运行时异常`

##### （二）返回包装对象

如何解决直接返回时可能存在的运行时异常呢？答案是使用返回包装类型，包装类型含有HTTP状态码，通过状态码来解析返回具体值。

使用方式跟上述相同，区别是将方法`getForObject`替换成`getForEntity`，方法参数使用完全一致。

![img_1.png](img_1.png)

> 实际开发过程中，强烈推荐使用Get请求使用 `getForEntity`方法，手动解析返回值!


### 二、Post请求

一般来说，使用POST请求，请求体都会带有参数，无需考虑不带参数的场景。

使用POST请求，主要构造`HttpEntity`请求体。
```java
public ResponseEntity<Object> test2(UserDTO userDTO) {
    String url = "http://localhost:8080/user/test";
    HttpEntity<UserDTO> request = new HttpEntity<>(userDTO);
    User user = restTemplate.postForObject(url, request, User.class);
    return ResponseEntity.ok(user);
}
```

> POST请求相较于GET请求稍微复杂一点，因为GET请求可以将请求参数放在url字符串中，POST请求需要单独构造请求体。

---

### 三、请求头参数

