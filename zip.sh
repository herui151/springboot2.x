#!/bin/bash
# shellcheck disable=SC2070
# sh zip.sh 097-springboot-rocketmq /Users/explore/B站素材/源代码/RocketMQ实战源代码
# 使用示例

if [ ! -n "${1}" ]; then
  echo "you have not input a string!"
else
  # 先把隐藏文件删除
  # shellcheck disable=SC2164
  cd "${1}"

  # 查找指定目录下指定的隐藏文件，并执行删除操作
  find $PWD -type f | grep -E ".DS_Store$" | xargs rm -rf
  # 删除临时文件
  find $PWD -type f | grep -E "(~\$)" | xargs rm -rf
  # 删除.idea文件
  find "$PWD" | grep '\.idea' | xargs rm -rf

  # shellcheck disable=SC2164
  # shellcheck disable=SC2103
  cd -
  # shellcheck disable=SC2034
  value=$((RANDOM * 1000))
  # shellcheck disable=SC2034
  pwd=${value:0:4}
#  zip -P ${pwd} -r /Users/explore/B站素材/源代码/MybatisPlus实战源代码/"${1}".zip "${1}"
#  echo ${pwd} > /Users/explore/B站素材/源代码/MybatisPlus实战源代码/解压密码.txt
  zip -P ${pwd} -r ${2}/"${1}".zip "${1}"
  echo ${pwd} > ${2}/解压密码.txt
fi