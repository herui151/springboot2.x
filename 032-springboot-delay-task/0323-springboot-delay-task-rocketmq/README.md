## RocketMQ延迟任务

RocketMQ很方便的实现延迟任务。

默认延迟等级
```text
1s 5s 10s 30s 1m 2m 3m 4m 5m 6m 7m 8m 9m 10m 20m 30m 1h 2h
```