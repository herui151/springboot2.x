## HuTool工具包数据脱敏

使用`hutool-core`包中的`DesensitizedUtil`工具类完成数据脱敏

![img.png](img.png)

### 具体步骤
引入依赖

```xml
<dependency>
    <groupId>cn.hutool</groupId>
    <artifactId>hutool-core</artifactId>
    <version>5.7.9</version>
</dependency>
```

使用`DesensitizedUtil`工具类，返回值一般有`普通对象`、`对象集合`、`分页对象`


### 关于HuTool的使用
对数据脱敏不熟悉，不知道如何实现，可以使用HuTool工具包来开发；

很多老司机不建议`生产环境`使用HuTool，大家根据自己实际情况来选择，量力而行。